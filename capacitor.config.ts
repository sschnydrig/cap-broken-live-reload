import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'app',
  webDir: 'dist/app',
  bundledWebRuntime: false,
  server: {
    url: '192.168.1.176:4200'
  }
};

export default config;
